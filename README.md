# LIS4381 - Mobile Web Application Development

## Benjamin Landerman

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create a healthy recipe application with Android Studio

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create petstore database
    - Create ticket value mobile app
    
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Add client-side validation to web app
    - Add images to carousel
    
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Add server-side validation to web app
    - Provide screenshots of web app

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create business card mobile app

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - PHP server-side validation
    - Prepared statements to help prevent SQL injection
    - Adds edit/delete functionality to A5
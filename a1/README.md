# LIS4381 - Mobile Web Application Development

## Benjamin Landerman

### Assignment 1 Requirements:

*Three parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installation
3. Chapter Questions (Ch. 1-2)

#### README.md file should include the following items:

* Screenshot of [AMPPS installation](http://localhost/cgi-bin/phpinfo.cgi)
* Screenshot of running [Java Hello](img/hello.png)
* Screenshot of running [Android Studio - My First App](img/app.png)
* git commands with short descriptions
* Bitbucket repo links a) this assignment and b) the completed tutorial above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit -  Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git clone - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of AMPPS running*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/hello.png)

*Screenshot of running Android Studio - My First App*:

![Android Studio Screenshot](img/app.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/bdl16/bitbucketstationlocations/ "Bitbucket Station Locations")
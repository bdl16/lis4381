> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications

## Benjamin Landerman

### Assignment 2 Requirements:

*Assignment Requirements:*

1. Create a mobile recipe app using Android Studio.

#### README.md file should include the following items:

* Screenshots of app's two interfaces.

#### Assignment Screenshots:

| *Screenshot of first user interface*          | *Screenshot of second user interface*         |
|:---------------------------------------------:|:---------------------------------------------:|
|![First](img/screen1.png)                      |![Second](img/screen2.png)                     |

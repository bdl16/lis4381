> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications

## Benjamin Landerman

### Assignment 3 Requirements:

*Assignment Requirements:*

1. Create petstore database.
2. Create ticket value mobile app.

#### README.md file should include the following items:

* Screenshots of app's two interfaces and ERD
* Links to a3.mwb and a3.sql

#### Assignment Screenshots:

| *Screenshot of first user interface*          | *Screenshot of second user interface*         |
|:---------------------------------------------:|:---------------------------------------------:|
|![First](img/screen1.png)                      |![Second](img/screen2.png)                     |

| *Screenshot of petstore ERD*                  |
|:---------------------------------------------:|
|![ERD](img/a3.png)                             |

#### Assignment Links:

[A3 MWB File](docs/a3.mwb "Link to A3 MWB")

[A3 SQL File](docs/a3.sql "Link to A3 SQL")
<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Benjamin Landerman">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment3</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					
				<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<b>Petstore Database (Entity Relationship Diagram):</b><br />
					<img src="img/a3.png" class="img-responsive" alt="A3 ERD" />

					<br /> <br />
					<b>MySQL Workbench and SQL Files:</b><br />
					<a href="docs/a3.mwb">Petstore MySQL Workbench File</a>
				<br />
					<a href="docs/a3.sql">Petstore SQL File</a>				

				</div>
			</div>

			<?php include_once "global/footer.php"; ?>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 <?php include_once("../js/include_js.php"); ?>		
 
</body>
</html>
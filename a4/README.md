> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications

## Benjamin Landerman

### Assignment 4 Requirements:

*Assignment Requirements:*

1. Add client-side validation to web app
2. Add images to carousel

#### README.md file should include the following items:

* Screenshots of home page, failed validation, passed validation
* Link to web app

#### Assignment Screenshots:

| *Screenshot of home page*                     |
|:---------------------------------------------:|
|![Home](img/home.png)                          |

| *Screenshot of failed validation*             | *Screenshot of passed validation*             |
|:---------------------------------------------:|:---------------------------------------------:|
|![Failed](img/failed.png)                      |![Passed](img/passed.png)                      |

#### Assignment Links:

[Link to web app](http://localhost/repos/lis4381/index.php "Link to web app")
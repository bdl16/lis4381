> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications

## Benjamin Landerman

### Assignment 5 Requirements:

*Assignment Requirements:*

1. Add server-side validation to web app
2. Provide screenshots of web app

#### README.md file should include the following items:

* Screenshots of index.php and add_petstore_process.php
* Link to web app

#### Assignment Screenshots:

| *Screenshot of index.php*                     | *Screenshot of add_petstore_process.php*      |
|:---------------------------------------------:|:---------------------------------------------:|
|![index.php](img/index.png)                    |![add petstore](img/error.png)                   |

#### Assignment Links:

[Link to web app](http://localhost/repos/lis4381/index.php "Link to web app")
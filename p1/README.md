> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications

## Benjamin Landerman

### Project 1 Requirements:

*Assignment Requirements:*

1. Create a business card application that includes name, photo, contact information, and interests

#### README.md file should include the following items:

* Screenshots of app's two interfaces

#### Assignment Screenshots:

| *Screenshot of first user interface*          | *Screenshot of second user interface*         |
|:---------------------------------------------:|:---------------------------------------------:|
|![First](img/screen1.png)                      |![Second](img/screen2.png)                     |
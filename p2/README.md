> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications

## Benjamin Landerman

### Project 2 Requirements:

*Assignment Requirements:*

1. PHP server-side validation
2. Prepared statements to help prevent SQL injection
3. Adds edit/delete functionality to A5

#### README.md file should include the following items:

* Screenshots of index.php, edit_petstore.php, edit_petstore_process.php, carousel (home page), and RSS feed
* Link to web app

#### Assignment Screenshots:

| *Screenshot of index.php*                     | *Screenshot of edit_petstore.php*             |
|:---------------------------------------------:|:---------------------------------------------:|
|![index.php](img/index.png)                    |![edit petstore](img/edit.png)                 |

| *Screenshot of edit_petstore_process.php*     | *Screenshot of Carousel (Home Page)*          |
|:---------------------------------------------:|:---------------------------------------------:|
|![error.php](img/error.png)                    |![carousel](img/carousel.png)                  |

| *Screenshot of RSS feed*                      |
|:---------------------------------------------:|
|![rss.php](img/rss.png)                        |

#### Assignment Links:

[Link to web app](http://localhost/repos/lis4381/index.php "Link to web app")
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Benjamin Landerman">
	<link rel="icon" href="favicon.ico">

		<title>LIS4381 - Simple Calculator</title>
		<?php include_once("../css/include_css_data_tables.php"); ?>
</head>
<body>

<?php include_once("../global/nav.php"); ?>
	
	<div class="container-fluid">
		 <div class="starter-template">
						<div class="page-header">
							<?php include_once("global/header.php"); ?>	
						</div>
    
<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

if(!empty($_POST)) {
    $num1 = $_POST['num1'];
    $num2 = $_POST['num2'];
    $operation = $_POST['operation'];
}


//code to process inserts goes here
if(preg_match('/^[-+]?[0-9]*\.?[0-9]+$/',$num1) && preg_match('/^[-+]?[0-9]*\.?[0-9]+$/',$num2)) {
    echo '<h2>' . "$operation" . '<h2>';
    /*
    if($operation == 'addition') {
        echo "$num1" . " + " . "$num2" . " = ";
        echo $num1 + $num2;
    }
    else if($operation == 'subtraction') {
        echo "$num1" . " - " . "$num2" . " = ";
        echo $num1 - $num2;
    }
    else if($operation == 'multiplcation') {
        echo "$num1" . " * " . "$num2" . " = ";
        echo $num1 * $num2;
    }
    else if($operation == 'division') {
        echo "$num1" . " / " . "$num2" . " = ";
        echo $num1 / $num2;
    }
    else if($operation == 'exponentiation') {
        echo "$num1" . " raised to the power of " . "$num2" . " = ";
        echo pow($num1, $num2);
    }
    */

    //switch example
    switch ($operation) {
        case "addition":
            echo "$num1" . " + " . "$num2" . " = ";
            echo $num1 + $num2;
        break;

        case "subtraction":
            echo "$num1" . " - " . "$num2" . " = ";
            echo $num1 - $num2;
        break;
        
        case "multiplication":
            echo "$num1" . " * " . "$num2" . " = ";
            echo $num1 * $num2;
        break;

        case "division":
            if($num2 == 0) {
                echo "Cannot divide by zero!";
            }
            else {
                echo "$num1" . " / " . "$num2" . " = ";
                echo $num1 / $num2;
            }
        break;

        case "exponentiation":
            echo "$num1" . " raised to the power of " . "$num2" . " = ";
            echo pow($num1, $num2);
        break;

        default:
            echo "Must select an operation";
    }
}?>
</body>